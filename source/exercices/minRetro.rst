Min Retrocompatible
------------------------

Écrivez une fonction ``min`` prenant en entrée deux entiers et renvoyant le plus petit des deux.

.. easypython:: /exercices/min/
   :language: python
   :titre: Min RetroComp
   :extra_yaml:
     fichier_ens: min.py
