Produit de deux entiers
------------------------

Écrivez une fonction ``min`` prenant en entrée deux entiers et renvoyant le plus petit des deux.

.. easypython:: /exercices/min/
   :language: Jacadi
   :titre: Min
   :extra_yaml:
     fichier_ens: min.py
